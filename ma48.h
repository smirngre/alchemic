C..............................................................................
C Common block to pass data to MA48 package:
C..............................................................................
C ma48cb1:
C nJ -- dimension of sparse Jacobian,
C LA = several times nJ,
C ICNTL, CNTL -- control parameters set in by MA48ID
C..............................................................................
      integer nJ, ICNTL, KEEP, LA
      real*8 CNTL
      dimension ICNTL(20), CNTL(10)
      COMMON /ma48cb1/ nJ, LA, ICNTL, CNTL
      