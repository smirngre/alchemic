MIT License

Copyright (c) 2019 Dmitry Semenov (MPIA, LMU); Grigorii Smirnov-Pinchukov (MPIA)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The scientific papers, which use the software, must cite the following paper:

@ARTICLE{2010A&A...522A..42S,
       author = {{Semenov}, D. and {Hersant}, F. and {Wakelam}, V. and 
         {Dutrey},A. and
         {Chapillon}, E. and {Guilloteau}, St. and {Henning}, Th. and
         {Launhardt}, R. and {Pi{\'e}tu}, V. and {Schreyer}, K.},
        title = "{Chemistry in disks. 
        IV. Benchmarking gas-grain chemical models with surface reactions}",
      journal = {\aap},
     keywords = {astrochemistry, molecular processes, molecular data, 
         methods: numerical, ISM: molecules, protoplanetary disks, 
         Astrophysics - Astrophysics of Galaxies, 
         Astrophysics - Instrumentation and Methods for Astrophysics},
        year = "2010",
        month = "Nov",
       volume = {522},
          eid = {A42},
        pages = {A42},
          doi = {10.1051/0004-6361/201015149},
archivePrefix = {arXiv},
       eprint = {1007.2302},
 primaryClass = {astro-ph.GA},
       adsurl = {https://ui.adsabs.harvard.edu/abs/2010A&A...522A..42S},
      adsnote = {Provided by the SAO/NASA Astrophysics Data System}
}


THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
