      SUBROUTINE Fcn(n,t,y,ydot,RPAR,IPAR)
      IMPLICIT NONE
	                                
C Global variable(s):
      INTEGER n, IPAR
      REAL*8 t, y, ydot, RPAR                                       
      DIMENSION y(*),ydot(n), RPAR(*), IPAR(*) 

C Common blocks:
      INCLUDE "parameters.h"
      INCLUDE "Fcn.h"
	                                           
C Local variable(s):
      INTEGER i, j, jr1, jr2, jp1, jp2, jp3, jp4, jp5, jrac1, jrac2,
     1   iBer, nBmax
      REAL*8 term, rracc0, rrdes0, rracc1, rrdes1, rrdiff0, rrdiff1, 
     1   rda_min0, rda_min1, sterm0, sterm1, cutoff, yy1, yy2, 
     2   yy1g, yy2g, fr
      CHARACTER*12 rr1, rr2, pp1
      LOGICAL surf_reac
      
C Initialization of the arrayes:
      ydot = 0.0D0

C Construct ODE system "on the fly":
C dy/dt:
      DO i = 1, nre
        rr1 = r1(i)
        rr2 = r2(i) 
        pp1 = p1(i)         
	jr1 = ir1(i)
	jr2 = ir2(i)
	jp1 = ip1(i)
	jp2 = ip2(i)
	jp3 = ip3(i)
	jp4 = ip4(i)
	jp5 = ip5(i)
	    
C Modified rate approach (Caselli et al. 1998):
        surf_reac = (rr2(1:1).EQ.'g').and.(surf_method(1:4).eq.'MRAT')   
        IF (surf_reac) THEN	         
     
	    rrdiff0 = Rdiff0(i)	  
	    rrdiff1 = Rdiff1(i)       
	    jrac1   = irac1(i)
  	    jrac2   = irac2(i)	
	    yy1g    = y(jrac1)/ddens
	    yy2g    = y(jrac2)/ddens	    
            yy1     = y(jr1)/ddens
            yy2     = y(jr2)/ddens  	    
	    sterm0  = rrdiff0
	    sterm1  = rrdiff1
	    
cd Transition function:	    
            if ((yy1.lt.2.0d0).and.(yy2.lt.2.0d0)) then
              fr = 1.0d0
            else if ((yy1.ge.2.0d0).and.(yy2.ge.2.0d0)) then
	      fr = 1.0d0 / (yy1-1.0d0) / (yy2-1.0d0)
	    else if (yy1.ge.2.0d0) then  
	      fr = 1.0d0 / (yy1-1.0d0)
	    else if (yy2.ge.2.0d0) then  
	      fr = 1.0d0 / (yy2-1.0d0)
	    endif	    	    	

C Homogeneous reactions:        
            if (jr1.eq.jr2) then
          
    	      rracc0   = Racc(iar1(i))*yy1g
	      rrdes0   = Rdes(idr1(i))
	      rda_min0 = DMAX1(rracc0,rrdes0)	        
              sterm0   = DMIN1(rrdiff0,rda_min0)
              sterm1   = sterm0
              
C Heterogeneous reactions:        
           else
          
	     rracc0   = Racc(iar1(i))*yy1g
	     rrdes0   = Rdes(idr1(i))
	     rda_min0 = DMAX1(rracc0,rrdes0)	        
             sterm0   = DMIN1(rrdiff0,rda_min0)	  
	     rracc1   = Racc(iar2(i))*yy2g     
	     rrdes1   = Rdes(idr2(i))
	     rda_min1 = DMAX1(rracc1,rrdes1)
	     sterm1   = DMIN1(rrdiff1,rda_min1)

           endif
          
C Final reaction rate is:
         ak(i) = (rrdiff0*(1.0d0-fr)+sterm0*fr+
     &            rrdiff1*(1.0d0-fr)+sterm1*fr)*ak_dens(i)          
     
      ENDIF

C Destruction of species: 
      IF (jr2.EQ.0) THEN     
	term = ak(i)*y(jr1) 
        ydot(jr1)=ydot(jr1)-term      
      ELSE
	term = ak(i)*y(jr1)*y(jr2)
        ydot(jr1)=ydot(jr1)-term
        ydot(jr2)=ydot(jr2)-term        
      END IF

C Formation of species:      
      ydot(jp1)=ydot(jp1)+term
      IF (jp2.NE.0) ydot(jp2)=ydot(jp2)+term
      IF (jp3.NE.0) ydot(jp3)=ydot(jp3)+term
      IF (jp4.NE.0) ydot(jp4)=ydot(jp4)+term
      IF (jp5.NE.0) ydot(jp5)=ydot(jp5)+term

C End loop 2 by reactions:
      END DO
      
C Exit:
      RETURN
      END
