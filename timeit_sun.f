      SUBROUTINE TIMEIT(CMSGTM,DTIME)
C***********************************************************************
C This version of timeit uses the system call "etime" available under
C SunOS and some other bsd-like Unix systems (e.g., Convex unix).
C
C Copyright (C) 1999 D.A.Semenov
C Copyright (C) 1993,1994,1995 B.T. Draine and P.J. Flatau
C This code is covered by the GNU General Public License.
C***********************************************************************
C 94.06.20 (PJF) add dtime to the formal parameters
C 95.06.19 (PJF) modified to suppress printing in CMSGTM='noprint'
C Arguments:
      CHARACTER CMSGTM*(*)
C
C External system calls:
      REAL*4 ETIME
cdima      EXTERNAL ETIME
C
C Local variables:
      REAL*4 DTIME,OLDTIM
      CHARACTER CSTA*3
      REAL*4 TARRAY(2)
C
C Local variables to be saved:
      SAVE CSTA,OLDTIM
C
C Data statements:
      DATA CSTA/'ONE'/
C
      IF(CSTA.EQ.'ONE')THEN
         CSTA='TWO'
         OLDTIM=ETIME(TARRAY)
c            print *,' System time is ', OLDTIM
      ELSEIF(CSTA.EQ.'TWO')THEN
         CSTA='ONE'
         DTIME=ETIME(TARRAY)-OLDTIM
c            print *,' System time is ', ETIME(TARRAY)
         IF(CMSGTM(1:7).NE.'NOPRINT'.AND.CMSGTM(1:7).NE.'noprint')THEN
c            print *,' Timing results for: ',CMSGTM
            print *,DTIME,' CPU time (in sec)'
         ENDIF
      ENDIF
      RETURN
      END

