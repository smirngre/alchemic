code_name=disk_chemistry_OSU06ggs_UV_D
source= $(code_name).f read_osu2.f   Drive_dvode.f  Fcn2.f Jacobian.f timeit_sun.f dvodpk2.f ma48_deps.f ma48.f  funs_dvodepk.f ma48_blas_deps.f
#source= $(code_name).f read_osu.f   Drive_dvode.f  Fcn.f Jacobian.f timeit_sun.f dvodpk2.f ma48_deps.f ma48.f  #funs_dvodepk.f ma48_blas_deps.f

#FC=ifort
FC=gfortran

formFLAG = -noauto -funroll-loops 
flowFLAG = #-check all 
optFLAG = -O3 -ipo -mp1 -xHost -i-dynamic #-O3 -march=pentium4 -mmmx -msse -msse2 -msse3 -funroll-loops
# Static executable (bigger size but somewhat faster):
#libFLAG= -L/home/semenov/intel/mkl/9.1/lib/em64t /home/semenov/intel/mkl/9.1/lib/em64t/libmkl_lapack.so -lmkl -lguide -pthread


ifeq (gfortran,${FC})
  formFLAG = -fno-automatic #-pg
  flowFLAG =  
  optFLAG = -O3  -march=native -funroll-loops -m64
  libFLAG =# -L/usr/lib/ -latlas -lblas -llapack 
  libFLAG = #-Wl,-framework -Wl,vecLib
endif

FLAGS =  $(optFLAG) $(formFLAG) $(flowFLAG) $(libFLAG)

$(code_name): $(source)
	@$(FC) $(FLAGS) $(source) -o $@
	@echo make complete

clean:
	rm -rf *~ *.o