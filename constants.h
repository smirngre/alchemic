C Dust albedo in UV:
      REAL*8 albedo_UV
      PARAMETER (albedo_UV=0.5d0)

C Constants (year in s, proton's mass, Boltzmann, Planck, Pi, gas mean molecular weight):
      REAL*8 year, aMp, aMe, ak_B, hp, pi, amu
      PARAMETER (year= 3.1536d+7, aMp=1.6724E-24, aMe=9.1095d-28,  
     1  ak_B=1.3807E-16,  hp=6.6262E-27, pi=3.141593E0, 
     2  amu= 1.43E0)
