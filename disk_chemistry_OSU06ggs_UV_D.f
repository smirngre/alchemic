C..............................................................................
C
C Program to model the chemical evolution under specified initial and physical 
c conditions
C
C..............................................................................
C
C copyright (c) 2002-2003, Dmitry Semenov, AIU Jena
C copyright (c) 2004-2012, Dmitry Semenov, MPIA Heidelberg
C
C..............................................................................
C
C Versions:
C
C 5.1 -> 6.0: update to the modified rate approach
C 6.0 -> 6.2: New MRE with Bessel-like distribution (Biham & Lipstat 2002)
C 6.2 -> 6.3: Updated photorates can be used now (van Dishoeck 2006)
C 6.3 -> 6.4: "Shell" parallelization is added (through getarg & bash script)
C 6.4 -> 6.5: Now intensity of UV ISRF can be set to arbitary value
C 6.6: Corrected branching of the surface reactions, new implementation of
C modified rate approach
C 7.0: ma28 is replaced by superior package ma48
C 8.1: Photodesorption rate is now correctly computed (13.10.2010)
C 8.2: Now abs. tolerance is made dependent on gas density (high for dense regions, low for atmosphere)
C 8.3: Multiple initial abundances files (1 per grid cells) can be used 
C..............................................................................
C
C Input parameter(s):  file: 0io.inp
C
C specs       == the name of a file contains names of chemical elements 
C                involved in a set of chemical reactions,
C
C rates       == the name of a file contains a system of chemical 
C                reactions for species from 'specs',
C
C out         == a name of output file contains calculated abundances,
C
C istart      == starting grid point,
C
C iend        == final grid point,
C
C rftype      == type of the radiation field: ISRF/TTau/Herb
C 
C Lya_part    == part of the UV flux from the Lyman alpha line
C
C UV_yield    == UV photodesorption yield (~10^-5 -- 10^-3)
C
C upho_file   == file name with updated photorates,
C
C UV_ISRF     == intensity of UV ISRF in units of the Draine's G-factor,
C
C scale_CRP   == a factor to scale the standard CRP ionization rate,
C
C zetaRN      == ionization rate due to decay of short-living radionuclides,
C
C scale_eD    == a factor to scale binding energies of surface molecules (except of H, H2, and D)
C
C input parameter(s):  file: 1environ_0d.inp
C
C Nr             == number of radial grid points,
C
C Nz             == number of vertical grid points,
C
C Rs(Nz)      == radial disk points (AU),
C
C Zs(Nz)      == vertical disk points (AU),
C 
C T(Nz)       == disk temperature [K],
C
C rho(Nz)     == disk density [g/cm^3],
C
C G0(Nz)      == stellar  rad. field in unit of the interstellar one,
C
C AvSt(Nz)    == a visual extinction, mag., in the direction on the star,
C
C AvIS(Nz)    == a visual interstellar extinction, mag.,
C
C ZetaCR(Nz)  == cosmic ray ionization rate,
C
C ZetaX(Nz)   == X-ray ionization rate,
C
C fH2(IS/St)  == shielding factor for H2 (Interstellar/Stellar components),
C
C fCO(IS/St)  == shielding factor for CO (Interstellar/Stellar components),
C
C aKdif(Nz)   == diffusion coefficient (in cm/s)
C
C input parameter(s):  file: 2times.inp
C
C tlast       == a time of the evolution of the system [years],
C
C tfirst      == first time step [years],
C
C nstep       == a number of time steps,
C
C input parameter(s):  file: 3abunds.inp
C
C nfrc        == dimension of initial values array 'frc',
C
C frc(1:nfrc) == initial amount of species 'y0(1:nfrc)' relative to H2,
C
C input parameter(s):  file: 4toleran.inp
C
C relerr      == a relative error of the calculations,
C
C abserr      == an absolute error of the calculations,
C
C
C input parameter(s):  file: 5surfchem.inp
C
C grain       == grain radius [cm],
C
C rhod        == grain density [g/cm^3],
C
C mdmg        == dust-to-gas mas ration (usually 1%),
C
C edconv      == ratio between desorption and diffusion energies,
C
C surf_des    == diff. to des. energy ratio, tunneling (yes|no),
C
C surf_method == method to handle surface chemistry modeling,
C
C material    == grain mantle composition,
C
C stoch_lim   == the lowest surface population when stochstic effects
C                become important,
C
C sprod       == relative fraction of products of surface reactions remaining
C                on dust surfaces upon formation  
C
C..............................................................................
C
C Output file(s):
C
C out.idl     == out written in a special format (absolute abundances!)
C
C..............................................................................
C
C Global parameter(s):
C
C nreac       == maximal amount of chemical reactions to be read,
C
C nspec       == maximal amount of chemical species to be considered,
C
C ntime       == maximal amount of taken time steps,
C 
C..............................................................................
C
C Common block(s):
C
C..............................................................................
C
C BL1:
C
C ns           == amount of species,
C
C s(ns)        == a species set,
C
C nre          == amount of reactions in the input file,
C
C index(nre)    == indices of the corresponding chemical reactions,
C
C r1(nre)       == names of the first reactants,
C
C ir1(nre)      == array of indices of 'r1' species in extended species set 's',
C
C r2(nre)       == names of the second reactants,
C
C ir2(nre)      == array of indices of 'r2' species in extended species set 's',
C
C p1(nre)       == names of the first products, 
C
C ip1(nre)      == array of indices of 'p1' species in extended species set 's',
C
C p2(nre)       == names of the second products, 
C
C ip2(nre)      == array of indices of 'p2' species in extended species set 's',
C
C p3(nre)       == names of the third products, 
C
C ip3(nre)      == array of indices of 'p3' species in extended species set 's',
C
C p4(nre)       == names of the fourth products, 
C
C ip4(nre)      == array of indices of 'p4' species in extended species set 's',
C
C p5(nr)        == names of the 5-th products, 
C
C ip5(nr)       == array of indices of 'p5' species in extended species set 's',
C
C alpha(nre)    == first components of the rate coeffs.,
C
C beta(nre)     == second components of the rate coeffs.,
C
C gamma(nre)    == third components of the rate coeffs.,
C
C ak(nre)       == rate coefficients, computed from 'alpha', 'beta' and 'gamma',
C
C ak_dH2        == rate coefficient of H + H|grain -> H2,
C
C..............................................................................
C
C Important variable(s):
C
C s(1:ns)     == an array with the names of species, 
C
C y(1:ns)     == an array with calculated number densities of species,
C
C gdens       == number density of hydrogen nucleii,
C
C ddens       == number density of dust grains,
C
C Stick       == sticking probability of species to be adsorbed on the dust,
C
C Stick_H     == sticking probability of H to be adsorbed on the dust,  
C
C Cion        == sticking probability of ions corrected for Coulomb attraction,
C
C Vth         == a thermal velocity for a chosen species,
C
C St          == a sticking probability for a chosen species
C
C..............................................................................
C
C Used function(s) (alphabetically): aweight
C
C Used subroutine(s) (alphabetically): abunds, ispecies, len_tri2, reads, readr
C 
C..............................................................................
      PROGRAM diskchemosu06
      IMPLICIT NONE

C Initialization of common blocks:
      INCLUDE "parameters.h"
      INCLUDE "Fcn.h"
      INCLUDE "constants.h"      

C Global variable(s):
      INTEGER nfrc, nstep, istart, iend, ncpu, icpu
      REAL*4 dtime0, dtime1, dtime2
      REAL*8 tlast, tfirst, relerr, abserr, frc, Rs, Zs, rhod, UV_yield,
     1  T, rho, G0, AvST, AvIS, yy, ZetaCR, ZetaX, dfact, Stick,Stick_H, 
     2  Cion, Vth, aweight, St, ak_photo, ak_therm, ak_crp, tcrp, mdmg,
     3  fh2_IS, fh2_St, fco_IS, fco_St, grain, abundances, Surf,
     4  Lya_part, sprod, UV_ISRF, scale_CRP, zetaRN, scale_Ed, Scale_X
      CHARACTER*4 surf_des, rftype, singleIA
      CHARACTER*7 gmat     
      CHARACTER*12 y0, rr1, rr2, pp1
      CHARACTER*132 specs, rates, upho_file, out, cmgtm, ver
      DIMENSION frc(nspec2),y0(nspec2), yy(nspec),
     1  abundances(nspec,ntime)

C Local variable(s):
      INTEGER i, j, l, nlen, Nr, Nz, nait, ipon, nlenr, iana, 
     1  iff, lx, is, li, lt, ind0, ind1, jcpu
      REAL*8 fa, Hr, anu0, anu1, amur, des0, des1, edes, asg, 
     1  akbar, edconv, ed_mod, ans, r_pah, Cion_pah, delta       
      CHARACTER*5 ait, siend
      CHARACTER*12 rrdes, sdes, sacc
      DIMENSION rrdes(nreac), edes(nreac), edconv(nreac),
     1 sdes(nreac),sacc(nreac)       

C Current version of the code:
      PARAMETER (ver = ' version 8.2 (24/07/2012)')

C Initialization of a driver routine to the integration of the ODEs:
      EXTERNAL drive, timeit

C Initialization of variables for 'timer':
      cmgtm = 'disk_chemistry,' //ver
cd	print *, cmgtm       

C Read total number of CPUs available for computations:
      call getarg(1,ait)
      read(ait,*) ncpu

C Read current CPU index:
      call getarg(2,ait)
      read (ait,*) icpu
      if (icpu.gt.ncpu) then
        write (*,*) 'Wrong CPU index: icpu>ncpu, stop'
        stop
      endif
C..............................................................................
C Read input data:
C 1) input & output file names:
C..............................................................................
      open (unit=01,file='0io.inp',status='old',
     &      access='sequential')
        read (01,*)  
        read (01,*) specs
        read (01,*) rates
        read (01,*) out
        
C Read species set from 'specs':
        CALL reads (specs)              
        CALL readr (rates)      
        
C Read position within the grid for which chemistry will be computed:
        read (01,*) istart
        if (istart.eq.0) istart = 1
        iend = 0
        read (01,*) siend
        if (siend.ne.'') read(siend,*) iend	

C Do we have better photorates?        
        read(01,*) rftype
        if (rftype.ne.'') then
	      read(01,*) Lya_part
	      if (Lya_part.gt.0.0d0) then
	        print *, 'Photoreactions with the Ly_alpha radiation'
	        print *, 'are not yet included, stop!'
	        stop
	      endif
	      read (01,*) UV_yield
	      read(01,*) upho_file	      
	      CALL update_photorates(rftype,Lya_part,upho_file)	      
        endif
C Rescaling factor of the interstellar UV RF:        
        read(01,*) UV_ISRF
C Rescaling factor of the CRP ionization rate:        
        read(01,*) scale_CRP
C Ionization due to short-lived radionuclides:
        read(01,*) zetaRN
C Rescaling factor of the desorption energies of surface species:       
        read(01,*) scale_Ed
C Rescaling factor of the X-ray ionization rate:        
        read(01,*) scale_X  
C Do we use a single initial abundance file or multiple (1 per a grid cell)?        
        singleIA = ''
        read(01,fmt=*,end=666) singleIA      
666     if (singleIA.eq.'') singleIA(1:3) = 'yes'   
      close (01)

C 4) coarse of time evolution:
      open (unit=01,file='2times.inp',status='old',
     &      access='sequential')
        read (01,*)   
        read (01,'(D9.3)') tlast
                   tlast = tlast * year
        read (01,'(D9.3)') tfirst
                  tfirst = tfirst * year
        read (01,'(I3)') nstep
      close (01)

C 5) tolerance request:
      open (unit=01,file='4toleran.inp',status='old',
     &      access='sequential')
        read (01,*)   
        read (01,'(D9.3)') relerr
        read (01,'(D9.3)') abserr
      close (01)   
      
C 6) Dust grain parameters & surface chemistry properties:
      open (unit=01,file='5surfchem.inp',status='old',
     &      access='sequential')
        read (01,*)   
        read (01,*) grain
        read (01,*) rhod
        read (01,*) mdmg
        read (01,*) ed_mod
        if (ed_mod.gt.1.0d0) then
          print *, ' ed_mod must be  < 1, exit!'
          STOP
        endif        
        read (01,*) surf_des
        read (01,*) surf_method  
        read (01,*) gmat
        read (01,*) stoch_lim
        read (01,*) sprod
        if ((sprod.gt.1.0d0).or.(sprod.lt.0.0d0)) then
          print *, ' sprod must be between 0 and 1, exit!'
          stop
        endif
      close (01) 
      
      Sads = asg(grain,gmat)

C 2) Read environment's conditions:
      open (unit=01,file='1environ_0d.inp',status='old',
     &      access='sequential')
        read (01,*)
        read (01,*) Nr
        read (01,*) Nz
        if (iend.gt.(Nr*Nz)) then
          write(*,*) 'Wrong position of final grid cell is given:'
          write(*,*) 'iend = ',iend,' > Nr*Nz = ',Nr*Nz,', stop!'
          stop
        endif

C Run static chemistry over a 1D grid:
      dtime2 = 0.0
      open(10,file='times')

      jcpu = istart + icpu - 1      
      DO i = 1, Nr*Nz, 1
      
        ipon = i
        IF (ipon.LE.9) THEN
          WRITE (ait,'(I1)') ipon
          nait = 1
        ELSE IF (ipon.LE.99) THEN
          WRITE (ait,'(I2)') ipon
          nait = 2
        ELSE  IF (ipon.LE.999) THEN
          WRITE (ait,'(I3)') ipon
          nait = 3
        ELSE  IF (ipon.LE.9999) THEN
          WRITE (ait,'(I4)') ipon
          nait = 4
        ELSE  
          WRITE (ait,'(I5)') ipon
          nait = 5
        END IF

C Read  physical conditions in a current grid cell:       
        read (01,*) Rs, Zs, rho, T, G0, AvSt, AvIS, ZetaCR, ZetaX,
     &    fh2_IS, fco_IS, fh2_St, fco_St, Hr
     
C Tmin shall not be lower than 9K:
        if (T.LE.9.0d0) T = 9.0d0          

c Run chemistry in selected grid cells only:
        if ((i.ne.jcpu).or.(i.gt.iend)) goto 999
        jcpu = jcpu + ncpu
        
c Ionization by short-living radionuclides:
        ZetaCR = ZetaCR*scale_CRP + ZetaRN
cd        print *, 'zeta_CRP = ',ZetaCR
        
C Rescale X-ray ionization rate/luminosity:
       ZetaX = ZetaX*scale_X
cd       print *, 'zeta_X = ', ZetaX      

C Read initial abundances (relative to total H) for a given disk cell:
       if (singleIA(1:3).EQ.'yes') then 
         open (unit=02,file='3abunds.inp',status='old',
     &      access='sequential')
           read (02,*) 
           read (02,*) nfrc  
           do j = 1, nfrc
             read (02,*) y0(j), frc(j)  
cd           write(*,*) y0(j), frc(j)  
           end do

       else  
         open (unit=02,file='3abunds.inp.'//ait,status='old',
     &      access='sequential')
           read (02,*) 
           read (02,*) nfrc  
           do j = 1, nfrc
             read (02,*) y0(j), frc(j)  
cd            write(*,*) y0(j), frc(j)  
           end do
       endif
       close(02)
       
C==============================================================================
C Start the disk chemistry calculations:
C==============================================================================
        WRITE(*,*) 'Grid point N ', i

        dtime0 = 0.0
        CALL timeit(cmgtm, dtime0)

C------------------------------------------------------------------------------
C Start computations in the z-direction.
C Loop 2 by vertical points:
C------------------------------------------------------------------------------
C Amount of dust grains is:
         ddens = rho * mdmg / (4.0D0/3.0D0*PI*grain*grain*grain*rhod)
         
C delta is the radius of the surface site:         
         delta = dsqrt(2.0d0/pi/ans(gmat))
cd         print *, delta
cd         print *, 2.0D8*delta*delta/4.0d0

C Compute a rate coefficient for production of H2 on
C dust surfaces (Hollenbach & McKee 1979):
cd         fa = 1D0 / (1D0 + 1D4*DEXP(-(6D2/T)))
cd         ak_dH2 = 3D-18*dsqrt(T)*fa /
cd     /   (1D0+4d-2*DSQRT(T+T)+2D-3*T+8D-6*T*T)

C Calculation of the sticking probabilities:
C Jones & Williams 1985 (Figure 2,3; the case of silicates):
         Stick = 1.0D0

C Hollenbach & McKee 1979:
         Stick_H = 1.0D0 / (1D0+4D-2*DSQRT(T+T)+2D-3*T+8D-6*T*T)

C Calculation of the long-range Coulomb attraction for ions and big grains:
         Cion = 1.0D0 + 1.671D-3 / grain / T

C Calculation of the rate coefficients:
      DO j = 1, nre
         ak(j) = 0.0d0      
         rr1 = r1(j)
         rr2 = r2(j)
         pp1 = p1(j)         
         
C a) Cosmic ray & X-ray ionization:
          IF (rr2.EQ.'CRP') THEN
            ak(j) = alpha(j)*zetaCR
            
          ELSE IF (rr2.EQ.'XRAYS') THEN
            ak(j) = alpha(j)*zetaX

C b) Photoreaction:
          ELSE IF (rr2.EQ.'PHOTON') THEN
          
            ak(j) = alpha(j)*dexp(-gamma(j)*AvSt)*G0+
     +              alpha_is(j)*dexp(-gamma_is(j)*AvIS)*UV_ISRF

C H2 shielding:
           IF (rr1.EQ.'H2') ak(j)= alpha(j)*G0*fh2_St*dexp(-2.5d0*AvSt)+
     +        alpha_is(j)*UV_ISRF*fh2_IS*dexp(-2.5d0*AvIS)
	      
C CO shielding:
           IF (rr1.EQ.'CO') ak(j)= alpha(j)*G0*fco_St+
     +       alpha_is(j)*UV_ISRF*fco_IS

C c) Cosmic ray induced photoionization:
          ELSE IF (rr2.EQ.'CRPHOT') THEN
            ak(j) = alpha(j)*(zetaCR+zetaX)  !/(1.0D0-albedo_UV)

C d) Neutralization reactions:
          ELSE IF (rr2.EQ.'G-') THEN     
             Vth = DSQRT(8.0D0*ak_B*T/PI/aweight(rr1)/aMp)
             ak(j) = alpha(j)*PI*grain*grain*Vth*Cion

C Ions plus neutral grains:
           ELSE IF (rr2.EQ.'G0') THEN     
             Vth = DSQRT(8.0D0*ak_B*T/PI/aweight(rr1)/aMp)
             ak(j) = alpha(j)*PI*grain*grain*Vth

C Collisions of the electrons with the neutral grains:
           ELSE IF (rr1.EQ.'G0') THEN     
             Vth = DSQRT(8.0D0*ak_B*T/PI/aMe)
             ak(j) = PI*grain*grain*Vth*1.329D0*DEXP(-T/20.0D0)

C Collisions of the electrons with the positively charged grains:
           ELSE IF (rr1.EQ.'G+') THEN     
             Vth = DSQRT(8.0D0*ak_B*T/PI/aMe)
             ak(j) = PI*grain*grain*Vth*1.329D0*DEXP(-T/20.0D0)*Cion
             
C g) Adsorption of the neutrals (if T is not too high):
           ELSE IF (rr2.EQ.'FREEZE')  THEN
             St = Stick
             IF (rr1.EQ.'H') St = Stick_H
             if (rr1.EQ.'H2') St = 0.0d0
             if (rr1.EQ.'HD') St = 0.0d0                         
             if (rr1.EQ.'D2') St = 0.0d0             
             
             Vth = DSQRT(8.0D0*ak_B*T/PI/aweight(rr1)/aMp)

             ak(j) = PI*grain*grain*Vth*ddens*St             
cd             ak(j) = alpha(j)*PI*grain*grain*Vth*ddens*St
cd             IF (rr1.EQ.'H') ak(j)=DMAX1(0.0D0,ak(j)-2.0D0*ak_dH2)


C Save a copy of the adsorption rates in the common block:             
             Racc(j) = ak(j)
             sacc(j) = pp1

C h) Desorption processes:
           ELSE IF (rr2.EQ.'DESORB') THEN

C Initialization of diffusion energy value according to the chosen
C model 'surf_des':           
             rrdes(j) = rr1
             edes(j) = gamma(j)*scale_Ed
             if (surf_des(1:4).eq.'HASG') then
               edconv(j) = 0.3d0
               if (rr1(1:3).eq.'gH ') edes(j) = 350.0d0
               if (rr1(1:4).eq.'gH2 ') edes(j) = 450.0d0
             else if (surf_des(1:4).eq.'RUFL') then
               edconv(j) = 0.77d0
             else if (surf_des(1:4).eq.'OBIH') then
               if (beta(j).ne.0.0d0) then
                 edconv(j) = beta(j)               
               else
                 edconv(j) = ed_mod
               endif
             else
               print *, 'Wrong input parameter, stop: ',surf_des
               stop
             endif           

           anu0 = DSQRT(ans(gmat)*ak_B/aMp*gamma(j)*scale_Ed/PI/PI/
     &         aweight(rr1))

C Photodesorption (UV photons + CRP-induced secondary UV photons, see Leger et al. 1985):
C The unshielded ISM UV flux is 2 10^8 [units?]:
           ak_photo = UV_yield*2.0D8*delta*delta/4.0d0*
     &         (G0*dexp(-2.0D0*AvSt)+dexp(-2.0D0*AvIS)*UV_ISRF+1D-5)
cd           ak_photo = UV_yield*PI*grain*grain*
cd     *         (G0*dexp(-2.0D0*AvSt)+dexp(-2.0D0*AvIS)*UV_ISRF+
cd     +          1.0d-5)

C Thermal desorption:
            ak_therm = anu0*DEXP(-gamma(j)*scale_Ed/T) 
            
C CRP desorption:
C Le'ger et al. 1985 (CRP heating of 1000A grain, Eq. 1a):
             tcrp = (4.36D5+T*T*T)**0.3333333333d0
             ak_crp = 2.431D-2*zetaCR*anu0*DEXP(-gamma(j)*scale_Ed/tcrp)
             
C Check if T is too high for a molecule to reside on a grain:
cd             if (gamma(j).gt.T*13.0d0) then            
                ak(j) = ak_photo + ak_therm + ak_crp             
cd             else
cd                ak(j) = 0.0d0
cd                call ispecies(rr1,nre,sacc,ind0)
cd                print *, rr1, ind0
cd                Racc(ind0) = 0.0d0
cd                ak(ind0) = 0.0d0 
cd             endif   
             
C Save a copy of the desorption rates in the common block:                          
             Rdes(j) = ak(j)
             sdes(j) = rr1                              
             
C i) Dust surface reaction:
           ELSE IF (rr2(1:1).EQ.'g') THEN 
              
C Initialize values of the diffusion energy for the reactants:           
             call ispecies(rr1,nre,rrdes,ind0)
             call ispecies(rr2,nre,rrdes,ind1)
             des0 = edes(ind0)
             des1 = edes(ind1)
             
C Compute their vibrational frequencies:    
             anu0 = DSQRT(ans(gmat)*ak_B/aMp*des0/PI/PI/aweight(rr1))
             anu1 = DSQRT(ans(gmat)*ak_B/aMp*des1/PI/PI/aweight(rr2))

C Compute diffusion rates of the reactants (thermal hopping and quantum tunneling for the model of HHL 1992):
             Rdiff0(j) = anu0*DEXP(-edconv(ind0)*des0/T)/Sads
             Rdiff1(j) = anu1*DEXP(-edconv(ind1)*des1/T)/Sads
             
             if (surf_des(1:4).eq.'HASG') then
               if ((rr1(1:3).eq.'gH ').or.(rr1(1:4).eq.'gH2 ')) then
                 amur = aweight(rr1)*aMp
                 akbar=DEXP(-4D0*PI/hp*1D-8*
     &               DSQRT(2D0*amur*edconv(ind0)*des0*ak_B))
                 Rdiff0(j) = anu0*akbar/Sads
               endif  
               if ((rr2(1:3).eq.'gH ').or.(rr2(1:4).eq.'gH2 ')) then
                 amur = aweight(rr2)*aMp
                 akbar=DEXP(-4D0*PI/hp*1D-8*
     *               DSQRT(2D0*amur*edconv(ind1)*des1*ak_B))
                 Rdiff1(j) = anu1*akbar/Sads               
               endif    
             endif 
	     
C Compute efficiency of the exothermic reactions (assuming quantum tunneling for H and H2 for the model of HHL 1992):             
             akbar= DEXP(-gamma(j)*scale_Ed/T)
             if (surf_des(1:4).eq.'HASG') then
              if ((rr1(1:3).eq.'gH ').or.(rr1(1:4).eq.'gH2 ').or.
     &            (rr2(1:3).eq.'gH ').or.(rr2(1:4).eq.'gH2 ')) then
                 amur = aweight(rr1)*aweight(rr2)/
     /              (aweight(rr1)+aweight(rr2))*aMp
                 akbar=DEXP(-4D0*PI/hp*1D-8*
     *                 DSQRT(2D0*amur*gamma(j)*scale_Ed*ak_B))
               endif
             endif  
              
             if (pp1(1:1).eq.'g') then  
               ak(j) = sprod*akbar*(Rdiff0(j)+Rdiff1(j))/ddens*alpha(j)
               ak_dens(j) = sprod*akbar/ddens*alpha(j)
             else
               ak(j) = (1.0d0-sprod)*akbar*(Rdiff0(j)+Rdiff1(j))/ddens*
     &                 alpha(j)
               ak_dens(j) = (1.0d0-sprod)*akbar/ddens*alpha(j)             
             endif  
             
C Initialize indices of the accretion and desorption rates for the both
C reactants:           
             call ispecies(rr1,nre,sacc,iar1(j))
             call ispecies(rr2,nre,sacc,iar2(j))             
             call ispecies(rr1,nre,sdes,idr1(j))
             call ispecies(rr2,nre,sdes,idr2(j))

C j) Two-body reaction:
           ELSE 
             ak(j) = alpha(j)*(T/300.0D0)**beta(j)*dexp(-gamma(j)/T)
        END IF 

      END DO
      
C Total gas particle density:      
      gdens = rho / aMp / amu 
      
C Initialise abundances at t=0:
      CALL abunds(ns,s,nfrc,y0,frc,gdens,ddens,yy)

C Update absolute tolerance wrt to gas density and compute chemistry:
      CALL drive(ns,tfirst,nstep,yy,tlast,relerr,abserr*gdens*1d-3,
     &   abundances)
      
C Add a tiny number to abundances to prevent it being smaller than 1d-99:
      abundances = dabs(abundances) + 1.0d-99      

C==============================================================================
C WRITE results to the output files:
C==============================================================================

C Open output file 'out':
        CALL len_tri2(out,132,nlen)
        OPEN (unit=20,file=out(1:nlen)//'_'//ait(1:nait)//'.out',
     &        status='unknown',access='append')
        REWIND 20        
        OPEN (unit=30,file=out(1:nlen)//'_'//ait(1:nait)//'.idl',
     &        status='unknown',access='append')
        REWIND 30

C 1) Write current parameters to the 'out//.idl':
        WRITE(30,'(A18)') '# ART input file: '
        WRITE(30,'(a80)') specs
        WRITE(30,'(a80)') rates        
        WRITE(30,'(I5)') ipon
        WRITE(30,'(1PE9.2)') Rs
        WRITE(30,'(1PE9.2)') Zs
        WRITE(30,'(0PF5.0)') T
        WRITE(30,'(1PE9.2)') rho
        WRITE(30,'(1PE9.2)') rhod
        WRITE(30,'(1PE9.2)') mdmg
        WRITE(30,'(1PE9.2)') grain
        WRITE(30,'(1PE9.2)') AvSt
        WRITE(30,'(1PE9.2)') AvIS
        WRITE(30,'(1PE9.2)') G0
        WRITE(30,'(1PE9.2)') ZetaCR
        WRITE(30,'(1PE9.2)') ZetaX
        WRITE(30,'(1PE9.2)') albedo_UV
        WRITE(30,'(1PE9.2)') tlast / year
        WRITE(30,'(1PE9.2)') tfirst / year
        WRITE(30,'(I4)') nfrc
        WRITE(30,'(A12,1x,D12.5)') (y0(j), frc(j), j = 1, nfrc)
        WRITE(30,*) ns
        WRITE(30,'((9(A12,2x)),:)') (s(j), j = 1, ns)
        write(30,*) nstep
        write(30,*) (times(j)/year, j = 1, nstep)
        WRITE(30,*) nre
        WRITE(30,'(10D12.5)') (ak(j), j = 1, nre)
        write(30,'(10D12.5)') ((abundances(j,l), l=1,nstep), j=1,ns)
	CLOSE(30)

C 2) Write current parameters to the 'out':
      li = ns
      lt = li+1
      iana =1
      write(20,20)
      write(20,75)
 75   format(21x,39('*'))
      write(20,84) lt-1
 84   format(20X,' **         DISK CHEMISTRY          **',/,
     * 20X,' **          ',1I3,' SPECIES SET          **',/,
     * 20X,' **            F77 VERSION            **',/,
     * 20X,' **             13/10/2010            **')
      write(20,75)
      write(20,20)
 21   format(1x,' SPECIES CONTAINED IN SCHEME: ',//)
      write(20,22) (s(j),j=1,ns)
 22   format(8(1x,a12))
      write(20,23)
 23   format(/)
      write(20,36) ns
 36   format(1x,' NUMBER OF VALID SPECIES USED = ',1i4)
      write(20,45) nre
 45   format(1x,' NUMBER OF VALID REACTIONS USED = ',1i6)
      write(20,23)
 20   format(//)

C Write input parameters:
      write(20,64)
 64   format(3x,' INITIAL VALUES: '/)
      WRITE(20,65) ipon, Rs, Zs, gdens, T, rho, 
     &     T, rhod, mdmg, grain, AvSt, AvIS, G0,
     &     (ZetaCR+ZetaX), albedo_UV, 
     &     times(1)/year, tlast/year, 0.0d0
 65   FORMAT(
     * 3X,' Grid point  = ',I5,/,
     * 3X,' Radius      = ',1PE11.3,' AU',/,
     * 3X,' Height      = ',1PE11.3,' AU',/,
     * 3X,' n(H+2H2)    = ',1PE11.3,' cm^(-3)',/,
     * 3X,' Tg          = ',0PF8.1,'K',/,
     * 3X,' rho_g       = ',1PE11.3,' g/cm^3',/,
     * 3X,' Td          = ',0PF8.1,'K',/,
     * 3X,' rho_d       = ',1PE11.3,' g/cm^3',/,
     * 3X,' Mdust/Mgas  = ',1PE11.3,/,
     * 3X,' grain size  = ',1PE11.3,' cm',/,
     * 3X,' Av(stellar) = ',1PE11.3,' mag.',/,
     * 3X,' Av(IS)      = ',1PE11.3,' mag.',/,
     * 3X,' G0(stellar) = ',1PE11.3,' G0(IS)',/,
     * 3X,' Zeta        = ',1PE11.3,/,
     * 3X,' albedo(UV)  = ',1PE11.3,/,
     * 3X,' Start time  = ',0PF9.0,' years',/,
     * 3X,' Finish time = ',0PF9.0,' years',/,
     * 3X,' Diff. coeff.= ',1PE11.3)
      WRITE(20,20)     
C Write calculated abundances:
      write(20,63)
63    format(3X,' CALCULATED ABUNDANCES: '/)
       is = 1
       iff = 6
32    write(20,41) (s(j),j=is,iff)
      write(20,76)
      do l = 1, nstep
         write(20,30) times(l)/year,(abundances(j,l)/gdens,j=is,iff)
      end do
30    format(1x,7(1pe11.3,3x))
      write(20,41)(s(j),j=is,iff)
41    format(6x,'time',8x,6(1a12,2x))
      write(20,20)
      is=is+6
      iff=iff+6
C Interupt?
      if (iff.gt.ns) iff=ns
      if (is.ge.li) go to 38
      go to 32
C Last output statement:
38    lx=li
      lt=lt-1
76    format(1x,104('-'))

      CLOSE (20)
cd      close (77)
cd      do j = 1, ns
cd       write(55,'(A10,3x,1pD13.5)'),s(j),abundances(j,nstep)/gdens
cd      enddo
      
C Print used CPU time:
      call timeit(cmgtm,dtime1)            
      dtime0 = dtime1 - dtime0
	dtime2 = dtime2 + dtime0
cdima      WRITE (*,*) ' '
      WRITE (10,*) 'CPU time: ', dtime0,'s'

999   CONTINUE

      END DO

C Print used CPU time:
cdima      WRITE (*,*) ' '
      WRITE (*, *) ' => Total time: ', dtime2,'s'
      WRITE (10,*) ' => Total time: ', dtime2,'s'

C Close all:
      close (01)
      close (10)
      

C Exit:
      END
C..............................................................................
C
C This subroutine computes initial abundances of chemical species for a
C given species set.
C
C..............................................................................
C
C Version 1.0 (11/12/2001)
C
C..............................................................................
C
C Input parameter(s):
C
C y(1:ny)        == the names of non-conserved species,
C
C yr(1:nr)       == the names of chemical species from '3abunds.inp',
C
C fraction(1:nr) == fractions of the corresponding species 'yr',
C
C density        == amount of hydrogen nuclei,
C
C ddens          == amount of dust grains
C 
C..............................................................................
C
C Output parameter(s):
C
C y0(1:ny)      == computed initial abundances
C
C..............................................................................
      SUBROUTINE abunds(ny,y,n,yr,fraction,density,ddens,y0)
      IMPLICIT NONE

C Global variable(s):
      INTEGER ny, n
      CHARACTER*12 y, yr
      REAL*8 fraction, density, y0, ddens
      DIMENSION y(ny), y0(ny), yr(n), fraction(n)

C Local variable(s):
      INTEGER i, j, k
      DIMENSION k(n)

C Initialization:
        do i = 1, ny
          y0(i) = 0.0D0 
        end do 
        do i = 1, n
          k(i) = 0
        end do 

C Search of species 'yr' among species 'y':
      i = 0
C A loop by 'yr':
10    continue
        i = i + 1
        if (i.gt.n)  goto 40  ! checked all 'yr', exit,

C A loop by 'y':
        j = 0
20      continue
        j = j + 1
        if (j.gt.ny) goto 10
        
C Found a necessary species:
        if (yr(i).eq.y(j)) then
          k(i) = j
          goto 10  ! switch to the next species 'yr',
        end if

        goto 20  ! switch to the next species 'y',

40    continue

C Calculation of the initial abundances:
      do i = 1, n
	  if (k(i).NE.0) then
	    print *,'Initially present is ',y(k(i))
          y0(k(i)) = fraction(i)*density          
          if ((y(k(i))(1:1).EQ.'G').and.(fraction(i).gt.0.999d0)) then
            y0(k(i)) = fraction(i)*ddens
          endif
        endif
      end do

C Exit:
      return
      end
C..............................................................................
C
C This subroutine returns index of a given species in a given array of species
C names.
C
C..............................................................................
C
C Version 1.0 (14/12/2001)
C
C..............................................................................
C
C Input parameter(s):
C
C species      == name of a given species,
C
C array(1:na) == a given array of species names,
C
C..............................................................................
C
C Output parameter(s):
C
C index      == index of 'species' among 'array(1:na)'
C
C..............................................................................
      SUBROUTINE ispecies(species,na,array,index)
      IMPLICIT NONE
      
C Global variable(s):
      INTEGER na, index
      CHARACTER*12 species, array
      DIMENSION array(na)
      
C Local variable(s):
      INTEGER i
      
C Start search of 'species' in 'array':
      i = 1
 10   CONTINUE
      IF (i.gt.na) GOTO 20      ! No 'species' in 'array', stop,
      IF (species.eq.array(i)) GOTO 30 ! Found 'species', exit,
         i = i + 1
         GOTO 10
C 'species' was not found in 'array', stop:
 20   CONTINUE
      index = 0
      RETURN
C 'Species' was found in 'array', initialize 'index':
 30   CONTINUE
      index = i
C Exit:
      RETURN
      END
C..............................................................................
C
C This subroutine returns a length of input character without final spaces.
C
C..............................................................................
C

C Version 1.0 (25/06/2001)
C
C..............................................................................
C
C Input parameter(s):
C
C x          == a input character,
C
C ixsize     == size of 'x',
C
C nlen       == a result length
C 
C..............................................................................
      subroutine len_tri2(x,ixsize,nlen)
      implicit none
C global variable(s):
      integer ixsize
      character*(*) x
      integer nlen
C local variable(s):
      integer i
      character*1 space
C invert loop to search first non-space character from end of "x":      
       i = ixsize
10      continue
         space = x(i:i)  ! read i letter from the end of 'x',
         if (space.ne.' ') goto 20  ! right length found, exit,
         i = i-1
         if (i.eq.0) goto 20  ! 'x' is empty, exit,
         goto 10
20      continue      
C result:
       nlen = i
C exit:
      return
      end             
C..............................................................................
C
C This function gives the surface density of adsorption sites X2 for various
C dust materials (see Biham et al. 2001).
C
C..............................................................................
C
C Version 1.1 (21.10.2006)
C
C..............................................................................
C
C Input parameter(s):
C 
C material  == grian mantle composition
C
C..............................................................................
C
C Output parameter(s):
C
C ans  == surface density of sites X 2
C
C..............................................................................
      REAL*8 FUNCTION ans(material)
      IMPLICIT NONE

C Global variable(s):
      character*7 material

C Surface density of sites x 2:
      if (material.EQ.'OLIVINE') then
        ans = 2.0d0*2.0d14  !sites cm^-2

      else if (material.EQ.'ACARBON') then
        ans = 2.0d0*5.0d13  !sites cm^-2

      else 
        write (*,*) 'ANS: Input material name is wrong: ', material
        stop
      endif

C Exit:
      RETURN
      END
C..............................................................................
C
C This function computes total number of surface sites on a spherical grain
C of a given radius, using surface density of sites of the relevant grain
C material
C
C..............................................................................
C
C Version 1.1 (21.10.2006)
C
C..............................................................................
C
C Input parameter(s):
C 
C radius    == grain radius in cm,
C
C material  == grain mantle composition
C
C..............................................................................
C
C Output parameter(s):
C
C ans  == total amount of surface sites available for adsorption
C
C..............................................................................
C
C Used functions: ans
C
C..............................................................................
      REAL*8 FUNCTION asg(radius,material)
      IMPLICIT NONE

C Pi:
      INCLUDE "constants.h"

C Global variable(s):
      real*8 radius
      character*7 material

C Surface density of sites:
      REAL*8 ans
      EXTERNAL ans

C Amount of adsorption sites on a grain:
      asg = 2.0d0*pi*radius*radius*ans(material)

C Exit:
      RETURN
      END
      
C..............................................................................
C
C This subroutine updates the UV photorates.
C
C..............................................................................
C
C Version 1.0 (06/11/2007)
C
C..............................................................................
C
C Input parameter(s):
C
C rftype     == type of the input radiation field,
C
C Ly_alpha   == a part of the UV flux in th Lyman alpha line,
C
C input      == a name of a file with updated photorates
C
C..............................................................................
C
C Common block(s):
C
C..............................................................................
C
C BL1:
C
C ns           == amount of species,
C
C s(ns)        == a species set,
C
C nre          == amount of reactions in the input file,
C
C index(nre)    == indices of the corresponding chemical reactions,
C
C r1(nre)       == names of the first reactants,
C
C ir1(nre)      == array of indices of 'r1' species in extended species set 's',
C
C r2(nre)       == names of the second reactants,
C
C ir2(nre)      == array of indices of 'r2' species in extended species set 's',
C
C p1(nre)       == names of the first products, 
C
C ip1(nre)      == array of indices of 'p1' species in extended species set 's',
C
C p2(nre)       == names of the second products, 
C
C ip2(nre)      == array of indices of 'p2' species in extended species set 's',
C
C p3(nre)       == names of the third products, 
C
C ip3(nre)      == array of indices of 'p3' species in extended species set 's',
C
C p4(nre)       == names of the fourth products, 
C
C ip4(nre)      == array of indices of 'p4' species in extended species set 's',
C
C p5(nr)        == names of the 5-th products, 
C
C ip5(nr)       == array of indices of 'p5' species in extended species set 's',
C
C alpha(nre)    == first components of the rate coeffs.,
C
C beta(nre)     == second components of the rate coeffs.,
C
C gamma(nre)    == third components of the rate coeffs.,
C
C ak(nre)       == rate coefficients, computed from 'alpha', 'beta' and 'gamma',
C
C..............................................................................
C
C Used subroutines(s) (alphabetically): len_tri2
C
C..............................................................................
      SUBROUTINE update_photorates(rftype,Lya_part,input)
      IMPLICIT NONE

C Global variable(s):
      REAL*8 Lya_part
      CHARACTER*4 rftype
      CHARACTER*80 input

C Initialization of common blocks:
      INCLUDE "parameters.h"
      INCLUDE "Fcn.h"
      
C Local variable(s):
      INTEGER i, j, nrupd, nlen
      REAL*8 rdata(7)
      CHARACTER*12 su, p1u, rr1, rr2, pp1
      LOGICAL update

C Read input file:
      CALL len_tri2(input,80,nlen)
      OPEN (unit=10,file=input(1:nlen),status='old',
     &      access='sequential')
      rewind 10
     
     
cd      open (55,file='upd_pd.txt')
cd      open (56,file='upd_pi.txt')
     
C I. Photodissociation updates:     
      read (10,*)
      read (10,*)      
      read (10,*)      
      read (10,*) nrupd   
      
cd      write (55,*) rftype
      
      
      do i = 1, nrupd
      
        read (10,*) su, p1u, (rdata(j),j=1,7)
cd        write (55,*) su, p1u, (rdata(j),j=1,7)
        
        do j = 1, nre
        
          rr1 = r1(j)
          rr2 = r2(j)
          pp1 = p1(j)
        
          update=( (rr1.eq.su).and.(pp1.eq.p1u).and.
     .             (rr2(1:6).eq.'PHOTON') )
          if (update.and.(ion_reac(j).eq.0)) then
          
cd        write (55,*), r1(j),' => ',p1(j),' ',p2(j),' ',p3(j)
cd        write (55,*), 'Old rate: ',alpha(j),' ',gamma(j)                
          
C Initialize ISRF rates:
              alpha_is(j) = rdata(1)
              gamma_is(j) = rdata(5)
              if (Lya_part.ne.0.0d0) beta_is(j) = rdata(4)
              
            if (rftype.eq.'ISRF') then              
              
              alpha(j) = alpha_is(j)
              gamma(j) = gamma_is(j)
              beta(j) = beta_is(j)            
            
            else if (rftype.eq.'Herb') then
            
              alpha(j) = rdata(2)
              gamma(j) = rdata(6)
              if (Lya_part.ne.0.0d0) beta(j) = rdata(4)            
            
            else if (rftype.eq.'TTau') then

              alpha(j) = rdata(3)
              gamma(j) = rdata(7)
              if (Lya_part.ne.0.0d0) beta(j) = rdata(4)            
            
            else 
            
              print *, 'PD: Wrong type of radiation field: ', rftype
              print *, 'stop!'
              stop
            
            endif
            
cd            write (55,*), 'New rate: ',alpha(j),' ',gamma(j)          
            
          
          endif
        
        enddo
        
      enddo

C II. Photoionization updates:     
      read (10,*)
      read (10,*)      
      read (10,*) nrupd   
           
cd      write (56,*) rftype
      
      do i = 1, nrupd
      
        read (10,*) su, (rdata(j),j=1,7)
cd        write (56,*) su, (rdata(j),j=1,7)
        
        do j = 1, nre
        
          rr1 = r1(j)
          rr2 = r2(j)
          
          update=((rr1.eq.su).and.(rr2(1:6).eq.'PHOTON'))
          if (update.and.(ion_reac(j).eq.1)) then
          
cd        write (56,*), r1(j),' => ',p1(j),' ',p2(j),' ',p3(j)
cd        write (56,*), 'Old rate: ',alpha(j),' ',gamma(j)          
          
C Initialize ISRF rates:
              alpha_is(j) = rdata(1)
              gamma_is(j) = rdata(5)
              if (Lya_part.ne.0.0d0) beta_is(j) = rdata(4)
              
            if (rftype.eq.'ISRF') then              
              
              alpha(j) = alpha_is(j)
              gamma(j) = gamma_is(j)
              beta(j) = beta_is(j)            
                        
            else if (rftype.eq.'Herb') then
            
              alpha(j) = rdata(2)
              gamma(j) = rdata(6)
              if (Lya_part.ne.0.0d0) beta(j) = rdata(4)            
            
            else if (rftype.eq.'TTau') then

              alpha(j) = rdata(3)
              gamma(j) = rdata(7)
              if (Lya_part.ne.0.0d0) beta(j) = rdata(4)            
            
            else 
            
              print *, 'PI: Wrong type of radiation field: ', rftype
              print *, 'stop!'
              stop
            
            endif
            
cd            write (56,*), 'New rate: ',alpha(j),' ',gamma(j)          

          
          endif
        
        enddo
        
      enddo

C Close all files:
      CLOSE (10)
      
cd      close (55)
cd      close (56)
      
C Exit:
      RETURN
      END      
C..............................................................................
C
C This function computes atomic weight of a given species.
C
C..............................................................................
C
C Version 1.0 (27/02/2002)
C
C..............................................................................
C
C Input parameter(s):
C 
C specs    == a name of species
C
C..............................................................................
C
C Output parameter(s):
C
C aweight  == computed atomic weight
C
C..............................................................................
      REAL*8 FUNCTION aweight(x)
      IMPLICIT NONE
      
C Global variable(s):
      CHARACTER*12 x

C Local variable(s):
      INTEGER i, first, last, n
      REAL*8 w1, w2
      CHARACTER*1 s1, tmp1, multi
      CHARACTER*2 s2, tmp2
      LOGICAL cond
      DIMENSION s1(7), w1(7), s2(6), w2(6)

C Initialization of the output:
        aweight = 0.0D0

C Initialization of atom's names:
        s1(1) = 'H'
        s1(2) = 'C'
        s1(3) = 'N'
        s1(4) = 'O'
        s1(5) = 'S'
        s1(6) = 'P'
        s1(7) = 'D'
        s2(1) = 'HE'
        s2(2) = 'FE'
        s2(3) = 'SI'
        s2(4) = 'NA'
        s2(5) = 'MG'
        s2(6) = 'CL'

C and their weights:
        w1(1) = 1.00E+00
        w1(2) = 1.20110E+01
        w1(3) = 1.40067E+01
        w1(4) = 1.59994E+01
        w1(5) = 3.20660E+01
        w1(6) = 3.09738E+01
        w1(7) = 2.0*w1(1)
        w2(1) = 4.00260E-00
        w2(2) = 5.58470E+01
        w2(3) = 2.80855E+01
        w2(4) = 2.29898E+01
        w2(5) = 2.43050E+01
        w2(6) = 3.54527E+01

C PAHs (assume 30 C-atoms and a few H):
        if ((x(1:3).eq.'PAH').or.(x(1:4).eq.'gPAH')) then
          aweight = 370.0d0
          return
        endif

C C10-bearing species:
        IF ((x(1:3).EQ.'C10').or.(x(1:4).EQ.'gC10')) THEN
          aweight = 120.11D+00
          RETURN
        END IF
        
C Initialization of bourders of the species name:
      CALL len_tri2(x,12,last)        

C Other cases:       
        first = 1
C Adsorbed species:
        IF (x(first:first).EQ.'g') THEN
          first = 2
C Ions:
        ELSE IF (x(last:last).EQ.'+') THEN
          last = last - 1
          IF (x(last:last).EQ.'+') last = last - 1
        ELSE IF (x(last:last).EQ.'-') THEN
          last = last - 1
        END IF

C Divide species onto atomic constituents:
 10   CONTINUE
        IF (first.GT.last) GOTO 20 ! checked the species,  exit,
C If it is not a last character in the species:
        IF ((last-first).GE.1) THEN
          tmp1 = x(first:first)
            tmp2 = x(first:(first+1))
              cond = .true.
C Start a search among two-letter atoms:
                DO i = 1, 6
                  IF (tmp2.EQ.s2(i)) THEN
                  aweight = aweight + w2(i)
                first = first + 2
              cond = .false.
            END IF       
          END DO 
C Start a search among one-letter atoms:
          IF (cond) THEN
            DO i = 1, 7
              IF (tmp1.EQ.s1(i)) THEN
                multi = tmp2(2:2)
C Find a possible multiplicators:
                                 n = 1
                       IF (multi.EQ.'2') THEN
                                 n = 2
                            first = first + 2
                  ELSE IF (multi.EQ.'3') THEN
                                 n = 3
                            first = first + 2
                  ELSE IF (multi.EQ.'4') THEN
                                 n = 4
                            first = first + 2
                  ELSE IF (multi.EQ.'5') THEN
                                 n = 5
                            first = first + 2
                  ELSE IF (multi.EQ.'6') THEN
                                 n = 6
                            first = first + 2
                  ELSE IF (multi.EQ.'7') THEN
                                 n = 7
                            first = first + 2
                  ELSE IF (multi.EQ.'8') THEN
                                 n = 8
                            first = first + 2
                  ELSE IF (multi.EQ.'9') THEN
                                 n = 9
                            first = first + 2
C There is no a multiplicator:
                  ELSE
                            first = first + 1                    
                  END IF
                aweight = aweight + n * w1(i)
              END IF
            END DO
          END IF  

C Checking the last character:
        ELSE
          DO i = 1, 7
            IF (x(first:last).EQ.s1(i)) THEN
              aweight = aweight + w1(i)
              first = first + 1
            END IF
          END DO 
        END IF

C Switch to the next character:         
      GOTO 10   
 20   CONTINUE

C Exit:
      RETURN
      END
      