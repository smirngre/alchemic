      subroutine Jacobian(F,n,t,y,ysv,rewt,fty,V,hrl1,JacS,IWP,IER,
     1     RPAR, IPAR)
      implicit none


c Global variable:
      integer n, IPAR, IWP, IER
      real*8 y, ysv, t, RPAR, rewt, fty, v, hrl1, JacS
      dimension y(*), IPAR(*), RPAR(*), ysv(*), rewt(*), fty(*), 
     1   V(*), JacS(*), IWP(*)

C Common blocks:
      INCLUDE "parameters.h"
      INCLUDE "Fcn.h"
      INCLUDE "ma48.h"
      INCLUDE "constants.h"      
	
C Local variables:
      character*2 suf
      integer i,j,k,jr1, jr2, jp1, jp2, jp3, jp4, jp5, l, m, 
     1  ir, iter, kk, info, iw, nJ0, ir0, ic0, JOB
      real*8 term1, term2, Jac, rinfo
      DIMENSION Jac(n,n),ir(n*nss),info(20),rinfo(10),
     1  iw(10*n),ir0(nspec*nss),ic0(nspec*nss)

C External function:
      EXTERNAL F
      SAVE nJ0, IR0, IC0
      
C Construct JAC "on the fly":
      kk = 0
      Jac = 0.0d0

C Loop by reactions:
      DO j = 1, nre
	jr1 = ir1(j)
	jr2 = ir2(j)
	jp1 = ip1(j)
	jp2 = ip2(j)
	jp3 = ip3(j)
	jp4 = ip4(j)
	jp5 = ip5(j)	  

C No second reactant:
        IF (jr2.EQ.0) THEN
	  term1 = ak(j)
	  if (term1.ne.0.0d0) then
  	    Jac(jr1,jr1) = Jac(jr1,jr1) - term1
	    Jac(jp1,jr1) = Jac(jp1,jr1) + term1
	    if (jp2.ne.0) Jac(jp2,jr1) = Jac(jp2,jr1) + term1
	    if (jp3.ne.0) Jac(jp3,jr1) = Jac(jp3,jr1) + term1
	    if (jp4.ne.0) Jac(jp4,jr1) = Jac(jp4,jr1) + term1
	    if (jp5.ne.0) Jac(jp5,jr1) = Jac(jp5,jr1) + term1	    
	  endif  
	ELSE
C Reactions with second reactant:
          term1 = ak(j)*y(jr1)
          term2 = ak(j)*y(jr2)
          if (term1.ne.0.0d0) then
            Jac(jr1,jr2) = Jac(jr1,jr2) - term1
            Jac(jr2,jr2) = Jac(jr2,jr2) - term1
            Jac(jp1,jr2) = Jac(jp1,jr2) + term1
            if (jp2.ne.0) Jac(jp2,jr2) = Jac(jp2,jr2) + term1
            if (jp3.ne.0) Jac(jp3,jr2) = Jac(jp3,jr2) + term1
            if (jp4.ne.0) Jac(jp4,jr2) = Jac(jp4,jr2) + term1
            if (jp5.ne.0) Jac(jp5,jr2) = Jac(jp5,jr2) + term1                    
          endif 
          if (term2.ne.0.0d0) then 
            Jac(jr1,jr1) = Jac(jr1,jr1) - term2
	    Jac(jr2,jr1) = Jac(jr2,jr1) - term2
	    Jac(jp1,jr1) = Jac(jp1,jr1) + term2
	    if (jp2.ne.0) Jac(jp2,jr1) = Jac(jp2,jr1) + term2
	    if (jp3.ne.0) Jac(jp3,jr1) = Jac(jp3,jr1) + term2
	    if (jp4.ne.0) Jac(jp4,jr1) = Jac(jp4,jr1) + term2
	    if (jp5.ne.0) Jac(jp5,jr1) = Jac(jp5,jr1) + term2
	  endif  
	END IF

C End loop by reactions:
      END DO
      
cd      term1 = ak_dH2*gdens
cd      Jac(iH,iH) = Jac(iH,iH) - 2.0d0*term1
cd	Jac(iH2,iH) = Jac(iH2,iH) + term1	

C Transform to -hrl1*Jac:
      Jac = -hrl1*Jac
      
C Add I matrix:
      do j = 1, n
        Jac(j,j) =Jac(j,j)+1.0d0
      enddo

C Initialize non-zero elements of the Newton matrix (I-Jac*hrl1):      
cd      write(551,*) 'nJ = ', nJ
      DO j = 1, n
	DO k = 1, n
	  if (Jac(j,k).ne.0.0d0) then	    
	    kk = kk + 1
	    JacS(kk)= Jac(j,k) 
            ir(kk)  = k
            IPAR(kk)  = j
cd            write(551,*) j,k,' ',JacS(kk)
	  endif
	ENDDO
      ENDDO
      

      nJ = kk
      LA = 10*nJ
      iter = iter + 1
      
      if (nJ0.ne.nJ) then               

C     Determine structure of JacS:      
        CALL MA48AD(n,n,nJ,1,LA,JacS,IPAR,ir,IWP,CNTL,ICNTL,IW,INFO,
     1    RINFO)
        IER = INFO(1)     
        IF (IER.LT.0) THEN
          WRITE (6,'(A,I3)') 'Error STOP from MA48A/AD with INFO(1) ='
     +      ,IER
          STOP
        END IF       
        
        JOB = 1
cd        nJ0 = nJ
        suf = ' I'
        IR0(1:nJ0) = IPAR(1:nJ)
        IC0(1:nJ0) = ir(1:nJ)
        
      else  

        IPAR(1:nJ) = IR0(1:nJ0)
        ir(1:nJ) = IC0(1:nJ0)
        JOB = 2
        suf = 'II'
        
      end if  

C     Factorize matrix
      CALL MA48BD(n,n,nJ,JOB,LA,JacS,IPAR,ir,IWP,CNTL,ICNTL,RPAR,IW,
     *        INFO,RINFO)
      IER = INFO(1)     
      IF (IER.NE.0) THEN
        WRITE (6,FMT='(A,I3/A)') 'STOP from MA48B/BD with INFO(1) =',
     +    IER,'Solution not possible'
        STOP
      END IF
      
cd      write (*,100), iter, t, hrl1, nJ, suf
100   format (1X,i7,2(1X,1pE13.6),1X,i8,1x,a2)      
      
      return
      end 
C
C Solution of the system Ax=b:
C
      SUBROUTINE PSOL(N, T, Y, FTY, WK, HRL1, WP, IWP, V, LR, IER,
     1                  RPAR, IPAR)

C Global variables:
      INTEGER IWP, IER, IPAR, N, LR
      DOUBLE PRECISION T, Y, FTY, WK, HRL1, WP, V, RPAR
      DIMENSION Y(*), FTY(*), WK(*), WP(*), IWP(*), V(*), 
     1	RPAR(*), IPAR(*)

C Common blocks:
      INCLUDE "parameters.h"
      INCLUDE "ma48.h"
      
C Local variables:
      integer INFO(20),IW(N)  
      real*8 ERROR(3)
      
C Direct solution of the system Ax=b:
      CALL MA48CD(N,N,.false.,1,LA,WP,IPAR,IWP,CNTL,ICNTL,
     *            V,V,ERROR,RPAR,IW,INFO)      
      IER = INFO(1)
      
      RETURN
      END
      