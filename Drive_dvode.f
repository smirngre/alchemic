C------------------------------------------------------------------------------
C "Driver" subroutine to the stiff SODE solver "DVODEPK".
C
C Version 29/01/2005
C
C------------------------------------------------------------------------------
C Input parameter(s):
C
C neq      == number of equations,
C
C tfirst   == initial time moment,
C
C nt       == amount of time steps to be taken,
C
C y(1:neq) == initial values,
C
C tlast    == last time moment,
C
C rtol1    == relative tolerance parameter,
C
C atol1    == absolute tolerance parameter,
C
C------------------------------------------------------------------------------
C Used subroutine(s) and function(s) (alphabetically):
C
C Function(s): Fcn, Jacobian
C Subroutine(s): Limex
C
C------------------------------------------------------------------------------
      subroutine drive(neq,tfirst,nt,yy,tlast,rtol1,atol1,abundances)
      implicit none

C Initialization of common blocks:
      INCLUDE "parameters.h"
      INCLUDE "Fcn.h"
      INCLUDE "dvode.h"
      INCLUDE "ma48.h"

C Global variable(s):
      integer neq, nt
      real*8 yy, tfirst, tlast, rtol1, atol1, abundances
      dimension yy(nspec),abundances(nspec,ntime)

C Local variable(s):
      integer mf, itol, itask, istate, iopt, lrw, liw, IPAR, i, k,
     1   neqz
      real*8 RPAR, t, tout, tstep, atol, rtol
      dimension RPAR(nss*10), IPAR(nspec*nss*2)

C External routines:
      external Fcn, Jacobian, psol

C Initial parameters:
         mf       = 21  !21/29 SPIGM/direct
         rtol     = rtol1
         atol     = atol1
         itol     = 1
         itask    = 1
         istate   = 1
         iopt     = 1
         neqz     = neq
         lrw      = neqz*nss+721+32*neqz  !Declared size of RWORK (61+17*n+LWP)
         liw      = 30+10*neqz             !Declared size of IWORK (30+LIWP)
	     IWORK(1) = neqz*nss              !LWP - size of real array for precond.
	     IWORK(2) = neqz*10                !LIWP - size of integer array for precond.
	     IWORK(3) = 1                     !JPRE=0,1,2,3
	     IWORK(4) = 1                     !JACFLG=0,1
	     IWORK(5) = 5  ! Max order to be allowed
	     IWORK(8) = 20  ! Max number of iterations in the SPIGMR
	     IWORK(9) = 20   ! Number of vectors on which orthogonalization is done
cd       RWORK(6) = 3.155d+08         ! maximum allowed step size is 10^3 years
         RWORK(8) = rtol  ! Convergence test constant, the default value is 0.05.
         t = 0.0d0
	     tout = tfirst
         tstep= 1.0D+01**(dlog10(tlast/tfirst) / (nt-1.0d0))

C     Set default controls for linear system solver ma48:
        CALL MA48ID(CNTL,ICNTL)

C Call DVODEPK:
      do i = 1, nt

        times(i) = tout
cdima        print *,'CALL DVODEPK: ',i
cd        write(*,10) 't0 = ',t / 3.155D+07
        write(*,10) 't1 = ',tout / 3.1536d+7
   10   FORMAT(1X,A5,1pE10.3)

   20   call dvodpk(Fcn,neqz,yy,t,tout,itol,rtol,atol,itask,
     &     istate,iopt,rwork,lrw,iwork,liw,Jacobian,psol,mf,rpar,ipar)

        tout = tout * tstep

C An error occured during the integration:
C If something was wrong, print warning:
C istate   == 2  if "dvode" was successful, negative otherwise,
C            -1 means excess work done on this call. (Perhaps wrong mf.)
C            -2 means excess accuracy requested. (Tolerances too small.)
C            -3 means illegal input detected. (See printed message.)
C            -4 means repeated error test failures. (Check all input.)
C            -5 means repeated convergence failures. (Perhaps bad
C               Jacobian supplied or wrong choice of mf or tolerances.)
C            -6 means error weight became zero during problem. (Solution
C               component i vanished, and atol or atol(i) = 0.)
C
      if (istate.lt.0) then
         write (*,*) 'istate = ',istate
      end if
      if (istate.eq.-1) then
         write(*,*) 'excessive amount of work was done, continue...'
         istate = 2
	   tout = tout / tstep
         goto 20
      else if (istate.eq.-2) then
         write(*,*) 'requested accuracy too much,'
         write(*,*) 'relative tolerance will increase by 2'
         rtol = rtol*2.0D0
         if (rtol.gt.1.0D-5) stop
         atol = 2.23D-16
         istate = 3
	   tout = tout / tstep
         goto 20
      else if (istate.eq.-3) then
         write(*,*) 'illegal input or infinite loop of calls, stop'
         stop
      else if (istate.eq.-4) then
         write(*,*) 'repeated error test failures (singularity?)'
         istate = 2
	   tout = tout / tstep
         goto 20
      else if (istate.eq.-5) then
         write(*,*) 'repeated convergence test failures,'
         write(*,*) 'perhaps Jacobian is not accurate enough'
         istate = 2
	   tout = tout / tstep
         goto 20
      else if (istate.eq.-6) then
         write(*,*) 'a solution component "i" is vanished, but'
         write(*,*) 'pure absolute error control ATOL = 0 was requested'
         istate = 2
	   tout = tout / tstep
         goto 20
      end if

C Save the current solution:
        do k = 1, neq
	    abundances(k,i) = yy(k)
	  enddo


C End loop by time moments:
	  enddo

C Exit:
      return
      end

