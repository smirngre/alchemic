C..............................................................................
C
C Common block(s):
C
C..............................................................................
C
C BL1:
C
C ns           == amount of species,
C
C s(ns)        == a species set,
C
C nr           == amount of reactions in the input file,
C
C index(nr)    == indices of the corresponding chemical reactions,
C
C r1(nr)       == names of the first reactants,
C
C ir1(nr)      == array of indices of 'r1' species in extended species set 's',
C
C r2(nr)       == names of the second reactants,
C
C ir2(nr)      == array of indices of 'r2' species in extended species set 's',
C
C p1(nr)       == names of the first products, 
C
C ip1(nr)      == array of indices of 'p1' species in extended species set 's',
C
C p2(nr)       == names of the second products, 
C
C ip2(nr)      == array of indices of 'p2' species in extended species set 's',
C
C p3(nr)       == names of the third products, 
C
C ip3(nr)      == array of indices of 'p3' species in extended species set 's',
C
C p4(nr)       == names of the fourth products, 
C
C ip4(nr)      == array of indices of 'p4' species in extended species set 's',
C
C p5(nr)       == names of the fifth products, 
C
C ip5(nr)      == array of indices of 'p5' species in extended species set 's',
C
C alpha(nr)    == first components of the rate coeffs.,
C
C beta(nr)     == second components of the rate coeffs.,
C
C gamma(nr)    == third components of the rate coeffs.,
C
C alpha_is(nr) == first components of the rate coeffs. for the ISRF,
C
C beta_is(nr)  == second components of the rate coeffs. for the ISRF,
C
C gamma_is(nr) == third components of the rate coeffs. for the ISRF,
C
C ak(nr)       == rate coefficients, computed from 'alpha', 'beta' and 'gamma',
C
C Rdiff,       == diffusion, accretion, and desorption rates for surface species
C Racc,
C Rdes
C
C iar, idr     == corresponding indices,
C
C ddens        == number density of small grains,
C
C irac         == returns an index of a gas-phase analoug to a surface species,
C
C ion_reac     == 0/1 flag denoting photoionization reaction,
C
C UV_field     == a string constant defining incident UV RF,
C
C nrc          == amounts of various reaction types,
C
C nr_surf      == total amount of surface reactions, 
C
C ind_surf     == indices of surface reactions,
C
C mrat         == .true. when surface chemistry is to be computed by
C                 modified rate approach
C
C..............................................................................
C Common block (rates, abundances, etc.):
      INTEGER ns, nre, index, ir1, ir2, ip1, ip2, ip3, ip4, igH, igH2,
     1  ip5, iar1, iar2, idr1, idr2, jar1, jar2, irac1, irac2, iHY,
     2  ion_reac, rtype, iH, iH2, nrc, ind_surf, nr_surf

      REAL*8 alpha, beta, gamma, alpha_is, beta_is, gamma_is, ak, times, 
     1  gdens, ddens, Rdiff0, Rdiff1, Racc, Rdes, ak_dens, stoch_lim, 
     2  Sads

      CHARACTER*4 surf_method
      CHARACTER*12 s, r1, r2, p1, p2, p3, p4, p5   
      
      LOGICAL mrat

      DIMENSION s(nspec), r1(nreac), r2(nreac), p1(nreac), p2(nreac), 
     1  p3(nreac), p4(nreac), index(nreac), alpha(nreac), beta(nreac), 
     2  gamma(nreac), ir1(nreac), ir2(nreac), ip1(nreac), ip2(nreac), 
     3  ip3(nreac), ip4(nreac), times(ntime), ak(nreac), Rdiff0(nreac),
     4  Rdiff1(nreac), Racc(nreac), Rdes(nreac), ak_dens(nreac),
     5  iar1(nreac), iar2(nreac), idr1(nreac), idr2(nreac), jar1(nreac), 
     6  jar2(nreac), ip5(nreac), p5(nreac), irac1(nreac), irac2(nreac),
     7  iHY(nreac), ion_reac(nreac), alpha_is(nreac), beta_is(nreac), 
     8  gamma_is(nreac), rtype(nreac), ind_surf(nreac), nrc(20)      


      COMMON /BL1/ ns, s, nre, index, ir1, ir2, ip1, ip2, ip3, ip4, 
     1  ip5, iar1, iar2, jar1, jar2, idr1, idr2, irac1, irac2, rtype, 
     2  ion_reac, r1, r2, gdens, ddens, p1, p2, p3, p4, p5, alpha, beta, 
     3  gamma, alpha_is, beta_is, gamma_is, ak, Rdiff0, Rdiff1, Racc, 
     4  Rdes, ak_dens, times, Sads, iH, iH2, iHY, igH, igH2, stoch_lim, 
     5  surf_method, ind_surf, nr_surf, nrc, mrat


