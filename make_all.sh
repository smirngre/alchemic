#!/bin/bash


# prepare io file for chemical simulations:
ln -fs ../$obs_file .
rm -rf 1*inp
cp 0io.inp.save  0io.inp
perl -pi -e "s/tmp/$chem_model/g" 0io.inp 
cd $current_run_dir/run1
nr=`head -n 3 1environ_0d.inp | tail -n 1 | awk "{print $3}"`
cd ../..
perl -pi -e "s/xxx/$nr/g" 0io.inp 

n_run=`ls -l $current_run_dir/ | grep run | wc -l`
icount=0
pdone=0.0

for x in $current_run_dir/run*; do
  
# prepare environmental file:
 ln -fs $x/1environ_0d.inp .
 ctime=`date`
 ((icount=icount+1))
 pdone=`echo "scale=4; $icount / $n_run * 100.0" | bc`
 echo $x' started at '$ctime', doing '$pdone'%'
 
# Calculate abundances: 
  sh execute_parallel.sh

# Extract a subset of abundances:
 ./extract $obs_file >&/dev/null&
  wait
  
# Gzip the output chemical files to save the HD space:
 gzip -f *.out
 gzip -f *.idl 

# Move it to another folder:
 mv -f $chem_model* ../Data/$x/. 
 

 
done